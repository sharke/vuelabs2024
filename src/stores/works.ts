import { ref  } from 'vue'
import { defineStore } from 'pinia'
// Type works
export type Work = {
  id: number
  title: string
  description: string
  images: string
}

// NB this api is on localy from Vue3ts_starter001 project
const apiWroksUrl = 'http://localhost:1515/works/'

export const useWorksStore = defineStore('Works', () => {
  // global store state
  const works = ref<Work[]>([])
  const currentWork = ref<Work | null>(null)

  // fetch all works
  async function fetchWorks() {
    try {
      const response = await fetch(apiWroksUrl)
      works.value = await response.json()
    } catch (e) {
      console.error('error fetchWorks')
      console.error(e)
      throw e
    }
  }

  return { works, fetchWorks }
})
