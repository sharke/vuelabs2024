import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AdminLayout from '../views/layouts/AdminLayout.vue'
import DefaultLayout from '../views/layouts/DefaultLayout.vue'
import AdminIndex from '../views/admin/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView,
      children: [
        {
          path: '/',
          name: 'DefaultLayout',
          component: DefaultLayout
        }
      ]
    },
    {
      path: '/admin/dashboard',
      component: AdminLayout,
      children: [
        {
          path: '/admin/dashboard',
          name: 'AdminIndex',
          component: AdminIndex
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
